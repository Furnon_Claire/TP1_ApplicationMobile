package com.tp1.uapv1602236.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class CountryView extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_view);

        /* Récupère le intent de mainActivity */

        Intent intent = getIntent();
        String country = intent.getStringExtra("Country");

        /* Création du pays récupérer en paramètre */

        final Country chosenCountry = CountryList.getCountry(country);

        /* Création de la vue du nom du pays */

        TextView name = (TextView) findViewById(R.id.CountryName);
        name.setText(country);

        /* Création de la vue du drapeau */

        ImageView flag = (ImageView) findViewById(R.id.CountryFlag);
        String src = chosenCountry.getmImgFile();
        if(src == "flag_of_france")
        {
            flag.setImageResource(R.drawable.flag_of_france);
        }
        else if(src == "flag_of_germany")
        {
            flag.setImageResource(R.drawable.flag_of_germany);
        }
        else if(src == "flag_of_spain")
        {
            flag.setImageResource(R.drawable.flag_of_spain);
        }
        else if(src == "flag_of_south_africa")
        {
            flag.setImageResource(R.drawable.flag_of_south_africa);
        }
        else if(src == "flag_of_the_united_states")
        {
            flag.setImageResource(R.drawable.flag_of_the_united_states);
        }
        else { flag.setImageResource(R.drawable.flag_of_japan);}

        /* Création des espaces à remplir pour les informations du pays */

        final EditText capital = (EditText) findViewById(R.id.editCountryCapitale);
        capital.setText(chosenCountry.getmCapital());

        final EditText language = (EditText) findViewById(R.id.editCountryLanguage);
        language.setText(chosenCountry.getmLanguage());

        final EditText money = (EditText) findViewById(R.id.editCountryMonney);
        money.setText(chosenCountry.getmCurrency());

        final EditText population = (EditText) findViewById(R.id.editCountryPop);
        population.setText(String.valueOf(chosenCountry.getmPopulation()));

        final EditText superficie = (EditText) findViewById(R.id.editCountrySuperficie);
        superficie.setText(String.valueOf(chosenCountry.getmArea() ));

        /* Création du bouton de sauvegarde */

        Button save = (Button) findViewById(R.id.buttonSave);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenCountry.setmCapital(capital.getText().toString());
                chosenCountry.setmLanguage(language.getText().toString());
                chosenCountry.setmCurrency(money.getText().toString());
                chosenCountry.setmPopulation(Integer.parseInt(population.getText().toString()));
                chosenCountry.setmArea(Integer.parseInt(superficie.getText().toString()));
                finish();
            }
        });

    }
}
