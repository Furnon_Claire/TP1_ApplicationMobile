package com.tp1.uapv1602236.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;

import static com.tp1.uapv1602236.myapplication.CountryList.getNameArray;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Récupération des pays */

        String[] Pays = getNameArray();

        /* Création de la liste */

        ListView listView = (ListView) findViewById(R.id.listePays);

        /* Création de l'ArrayAdapter pour lier la map et la liste */

        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Pays);

        listView.setAdapter(itemsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, CountryView.class);
                intent.putExtra("Country", item);
                startActivity(intent);
            }
        });

    }
}
